import sys


def capital(x):
    if not isinstance(x, str):
        raise TypeError('non string type')
    return str(x).capitalize()
