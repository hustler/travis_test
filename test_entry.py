import pytest
from entry import capital


def test_capital():
    assert capital('test') == "Test"


def test_raises_exception_on_non_string_argument():
    with pytest.raises(TypeError):
        capital(0)
